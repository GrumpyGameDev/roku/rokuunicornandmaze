Function Main() As Void
    machine = CreateMachine()
    screen = CreateObject("roScreen", true)
    port = CreateObject("roMessagePort")
    context = CreateContext(port)
    screen.SetMessagePort(port)
    machine[machine.current].start(context)
    done = false
    While not done
        machine[machine.current].draw(context,context.resources.bitmaps.buffer)
        offsetX=((100-context.data.zoom)/100)*(screen.GetWidth()/2)
        offsetY=((100-context.data.zoom)/100)*(screen.GetHeight()/2)
        scaleX = screen.GetWidth()/context.resources.bitmaps.buffer.GetWidth()*context.data.zoom/100
        scaleY = screen.GetHeight()/context.resources.bitmaps.buffer.GetHeight()*context.data.zoom/100
        screen.clear(&h000000ff)
        screen.DrawScaledObject(offsetX,offsetY,scaleX,scaleY,context.resources.bitmaps.buffer)
        screen.SwapBuffers()
        event = port.GetMessage()
        nextState = machine[machine.current].handle(context, event)
        If (nextState="")
            done = true
        Else If (nextState<>machine.current)
            machine[machine.current].finish(context)
            machine.current = nextState
            machine[machine.current].start(context)
        End If
    End While
End Function

