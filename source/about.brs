Function CreateAbout() As Object
    Return {
        fontname:"default",
        backgroundcolor:&h000000FF,
        textcolor:&hebebebff,
        titleColor:&h7575ebff,
        items:["By Ernest Pazera",
        "With help from Gwen, Owen, and Rose.",
        "",
        "'Spiked dragon head','Unicorn','Stairs'",
        "by delapoite under CC-BY 3",
        "https://delapouite.com/",
        "",
        "'Rainbow star' by Lorc under CC-BY 3"
        "http://lorcblog.blogspot.com/"],

        start: Function(context) As Void
        End Function,

        finish: Function (context) As Void
        End Function,

        draw: Function(context,screen) As Void
            lineHeight = context.resources.fonts[m.fontname].GetOneLineHeight() 
            screen.clear(m.backgroundcolor)
            screen.DrawObject(0,0,context.resources.bitmaps.background)
            y=0
            index=0
            screen.DrawText("About Unicorn and Star", 0, y, m.titleColor, context.resources.fonts[m.fontname])
            y = y + lineHeight
            for each item in m.items
                screen.DrawText(item, 0, y, m.textcolor, context.resources.fonts[m.fontname])
                y = y + lineHeight
            end for
        End Function,

        handle: Function (context, event) As String
            if(type(event)="roUniversalControlEvent")
                button = event.GetInt()
                if(button=0)'back
                    return "mainmenu"
                else if(button=8)
                    context.data.zoom-=5
                    if context.data.zoom<80 then
                        context.data.zoom=80
                    end if
                else if(button=9)
                    context.data.zoom+=5
                    if context.data.zoom>100 then
                        context.data.zoom=100
                    end if
                else if(button=13)
                    context.data.mute = not context.data.mute
                    ApplyMute(context)
                end if
            end if
            Return "about"
        End Function
    }
End Function