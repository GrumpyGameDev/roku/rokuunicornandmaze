Function CreateConfirmQuit() As Object
    Return {
        items:["No", "Yes"],
        fontname:"default",
        backgroundcolor:&h000000FF,
        inactivecolor:&hebebebff,
        activecolor:&hebeb00ff,
        promptColor:&h7575ebff,
        currentIndex:0,
        prompt:"Are you sure?",

        start: Function(context) As Void
        End Function,

        finish: Function (context) As Void
        End Function,

        draw: Function(context,screen) As Void
            lineHeight = context.resources.fonts[m.fontname].GetOneLineHeight() 
            font = context.resources.fonts[m.fontname]
            screen.clear(m.backgroundcolor)
            screen.DrawObject(0,0,context.resources.bitmaps.background)
            y=screen.GetHeight()/2-(lineHeight*3/2)
            screen.DrawText(m.prompt, screen.GetWidth()/2-font.GetOneLineWidth(m.prompt,screen.GetWidth())/2, y, m.promptColor, font)
            y = y + lineHeight
            index=0
            for each item in m.items
                if (index=m.currentIndex)
                    screen.DrawText(item, screen.GetWidth()/2-font.GetOneLineWidth(item,screen.GetWidth())/2, y, m.activecolor, font)
                else
                    screen.DrawText(item, screen.GetWidth()/2-font.GetOneLineWidth(item,screen.GetWidth())/2, y, m.inactivecolor, font)
                end if
                index=index+1
                y = y + lineHeight
            end for
        End Function,

        handle: Function (context, event) As String
            if(type(event)="roUniversalControlEvent")
                button = event.GetInt()
                if(button=2)'up
                    m.currentIndex=(m.currentIndex+m.items.Count()-1) MOD m.items.Count()
                else if(button=3)'down
                    m.currentIndex=(m.currentIndex+1) MOD m.items.Count()
                else if(button=6)'select
                    if(m.currentIndex=1)
                        return ""
                    else
                        return "mainmenu"
                    end if
                else if(button=0)'back
                    return "mainmenu"
                else if(button=8)
                    context.data.zoom-=5
                    if context.data.zoom<80 then
                        context.data.zoom=80
                    end if
                else if(button=9)
                    context.data.zoom+=5
                    if context.data.zoom>100 then
                        context.data.zoom=100
                    end if
                else if(button=13)
                    context.data.mute = not context.data.mute
                    ApplyMute(context)
                end if
            end if
            Return "confirmquit"
        End Function
    }
End Function