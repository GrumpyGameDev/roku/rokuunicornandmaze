Function CreateMazeWalker() As Object
    Return {
        values:["north","east","south","west"],
        opposites:{
            "north":"south",
            "east":"west",
            "south":"north",
            "west":"east"
        },
        deltas:{
            "north":{x:0,y:-1},
            "east":{x:1,y:0},
            "south":{x:0,y:1},
            "west":{x:-1,y:0},
        }
    }
End Function

Function CreateMazeDoor() As Object
    Return {
        open:false
        clear:function()
            m.open=false
        end function
    }
End Function

Function CreateMazeCell() As Object
    Return {
        neighbors:{},
        doors:{},
        state:"outside",
        clear:function ()
            m.state="outside"
            for each door in m.doors
                m.doors[door].clear()
            end for
        end function,
        isdeadend:function()
            tally=0
            for each door in m.doors
                if m.doors[door].open then
                    tally++
                end if
            end for
            return tally=1
        end function
    }
End Function

Function CreateMazeColumn(rows as Integer) As Object
    column = []
    While (rows>0)
        rows = rows - 1
        column.Push(CreateMazeCell())
    End While
    Return column
End Function

Function CreateMaze(columns As Integer, rows as Integer, walker As Object) As Object
    maze = {
        columns:columns,
        rows:rows,
        walker:walker,
        clear:function()
            for column=0 to m.columns-1
                for row=0 to m.rows-1
                    m.cells[column][row].clear()
                end for
            end for
        end function,
        generate:Function ()
            m.clear()
            column=Rnd(m.columns)-1
            row = Rnd(m.rows)-1
            cell = m.cells[column][row]
            cell.state="inside"
            frontier=[]
            for each direction in cell.neighbors.keys()
                nextcell = cell.neighbors[direction]
                nextcell.state="frontier"
                frontier.push(nextcell)
            end for
            while (frontier.Count()>0)
                index=Rnd(frontier.Count())-1
                cell = frontier[index]
                frontier.delete(index)
                options=[]
                for each direction in cell.neighbors.keys()
                    nextcell = cell.neighbors[direction]
                    if (nextcell.state="inside")
                        options.push(direction)
                    end if
                end for
                direction = options[Rnd(options.Count())-1]
                cell.doors[direction].open=true
                cell.state="inside"
                for each direction in cell.neighbors.keys()
                    nextcell = cell.neighbors[direction]
                    if(nextcell.state="outside")
                        nextcell.state="frontier"
                        frontier.push(nextcell)
                    end if
                end for
            end while
        End Function
    }
    maze.cells=[]
    while(maze.cells.Count()<columns)
        maze.cells.push(CreateMazeColumn(rows))
    end while
    for column=0 to maze.columns-1
        for row=0 to maze.rows-1
            cell = maze.cells[column][row]
            for each direction in walker.values
                delta = walker.deltas[direction]
                nextcolumn=column+delta.x
                nextrow=row+delta.y
                if ((not cell.neighbors.doesexist(direction)) and nextcolumn>=0 and nextcolumn<maze.columns and nextrow>=0 and nextrow<maze.rows)
                    nextcell = maze.cells[nextcolumn][nextrow]
                    opposite = walker.opposites[direction]
                    door = CreateMazeDoor()
                    cell.doors[direction]=door
                    nextcell.doors[opposite]=door
                    cell.neighbors[direction]=nextcell
                    nextcell.neighbors[opposite]=cell
                end if
            end for
        end for
    end for
    return maze
End Function