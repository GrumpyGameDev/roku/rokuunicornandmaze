Function CreateMachine() As Object
    Return {
        current : "mainmenu",
        mainmenu: CreateMainMenu(),
        confirmquit: CreateConfirmQuit(),
        howtoplay: CreateHowToPlay(),
        about:CreateAbout(),
        play: CreatePlay()
    }
End Function