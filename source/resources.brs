Function CreateResources(port)
    resources = {}
    resources.fontRegistry = CreateObject("roFontRegistry")
    resources.fonts = {}
    resources.fonts.default = resources.fontRegistry.GetDefaultFont()
    resources.bitmaps = {}
    resources.bitmaps.background=CreateObject("roBitmap","pkg:/images/background.png")
    resources.bitmaps.unicorn=CreateObject("roBitmap","pkg:/images/unicorn.png")
    resources.bitmaps.star=CreateObject("roBitmap","pkg:/images/rainbow-star.png")
    resources.bitmaps.stairs=CreateObject("roBitmap","pkg:/images/stairs.png")
    resources.bitmaps.dragon=CreateObject("roBitmap","pkg:/images/spiked-dragon-head.png")
    resources.bitmaps.buffer = CreateObject("roBitmap",{width:1280,height:720,AlphaEnable:false})
    resources.audio={}
    resources.audio.dragon=CreateObject("roAudioResource","pkg:/assets/Dragon.wav")
    resources.audio.stairs=CreateObject("roAudioResource","pkg:/assets/Stairs.wav")
    resources.audio.star=CreateObject("roAudioResource","pkg:/assets/Star.wav")
    resources.audioplayer=CreateObject("roAudioPlayer")
    resources.audioplayer.SetMessagePort(port)
    song = CreateObject("roAssociativeArray")
    song.url = "https://pdgrokuaudio.azurewebsites.net/Komiku_-_28_-_Jenifer_The_Game.mp3"
    resources.audioplayer.addcontent(song)
    resources.audioplayer.setloop(true)
    resources.audioplayer.play()
    return resources
End Function
