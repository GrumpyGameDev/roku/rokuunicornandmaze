Function CreateHowToPlay() As Object
    Return {
        fontname:"default",
        backgroundcolor:&h000000FF,
        textcolor:&hebebebff,
        titleColor:&h7575ebff,
        items:["You go through a maze using up, down, left, and right!",
        "Collect all of the stars for the stairs to appear.",
        "Watch out for the dragon, who will make more stars appear.",
        "",
        "Controls:",
        "Move: Arrows",
        "Zoom: Rewind and Fast Forward",
        "Mute: Play/Pause"],

        start: Function(context) As Void
        End Function,

        finish: Function (context) As Void
        End Function,

        draw: Function(context,screen) As Void
            lineHeight = context.resources.fonts[m.fontname].GetOneLineHeight() 
            screen.clear(m.backgroundcolor)
            screen.DrawObject(0,0,context.resources.bitmaps.background)
            y=0
            index=0
            screen.DrawText("How to Play", 0, y, m.titleColor, context.resources.fonts[m.fontname])
            y = y + lineHeight
            for each item in m.items
                screen.DrawText(item, 0, y, m.textcolor, context.resources.fonts[m.fontname])
                y = y + lineHeight
            end for
        End Function,

        handle: Function (context, event) As String
            if(type(event)="roUniversalControlEvent")
                button = event.GetInt()
                if(button=0)'back
                    return "mainmenu"
                else if(button=8)
                    context.data.zoom-=5
                    if context.data.zoom<80 then
                        context.data.zoom=80
                    end if
                else if(button=9)
                    context.data.zoom+=5
                    if context.data.zoom>100 then
                        context.data.zoom=100
                    end if
                else if(button=13)
                    context.data.mute = not context.data.mute
                    ApplyMute(context)
                end if
            end if
            Return "howtoplay"
        End Function
    }
End Function