Function CreateData()
    data= {
        columns:16,
        rows:9,
        cellwidth:80,
        cellheight:80,
        unicorn:{x:0,y:0},
        timer:0,
        zoom:100,
        mute:false
    }
    data.maze = CreateMaze(data.columns, data.rows, CreateMazeWalker())
    data.toggledoors=GetToggleDoors(data.maze)
    data.dragon = {x:0, y:data.rows-1}
    return data
End Function
Function GetToggleDoors(maze)
    result=[]
    for column=0 to maze.columns-1
        for row=0 to maze.rows-1
            cell = maze.cells[column][row]
            if cell.isdeadend()
                options=[]
                for each direction in cell.doors.keys()
                    if not cell.doors[direction].open then
                        options.push(direction)
                    end if
                end for
                direction = options[Rnd(options.Count())-1]
                cell.doors[direction].open = true
                result.push(cell.doors[direction])
            end if
        end for
    end for
    for each door in result
        door.open = false
    end for
    return result
End Function
Function NewLevel(data,level)
    if (level>(data.rows*data.columns-1))
        level=data.rows*data.columns-1
    end if
    for column=0 to data.columns-1
        for row=0 to data.rows-1
            data.maze.cells[column][row].star=false
            data.maze.cells[column][row].stairs=false
        end for
    end for
    data.maze.generate()
    data.toggledoors=GetToggleDoors(data.maze)
    data.level=0
    while(data.level<level)
        column=Rnd(data.columns)-1
        row=Rnd(data.rows)-1
        if(column<>data.unicorn.x and row<>data.unicorn.y and not data.maze.cells[column][row].star)
            data.maze.cells[column][row].star=true
            data.level++
        end if
    end while
    data.starsleft=data.level
    UpdatePathfinding(data)
End Function
Function NextLevel(data)
    NewLevel(data,data.level+1)
End Function
Function PlaceStairs(data)
    column=data.unicorn.x
    row=data.unicorn.y
    while(column=data.unicorn.x and row=data.unicorn.y)
        column=Rnd(data.columns)-1
        row=Rnd(data.rows)-1
    end while
    data.maze.cells[column][row].stairs=true
End Function
Function UpdatePathfinding(data)
    for column=0 to data.columns-1
        for row=0 to data.rows-1
            data.maze.cells[column][row].pathcost=9999
        end for
    end for
    SetPathCost(data.maze,data.unicorn.x,data.unicorn.y,0)
End Function
Function SetPathCost(maze,x,y,value)
    stack=[]
    stack.push({x:x,y:y,value:value})
    while (stack.Count()>0)
        index = rnd(stack.count())-1
        item = stack[index]
        stack.delete(index)
        if(item.value<maze.cells[item.x][item.y].pathcost)
            maze.cells[item.x][item.y].pathcost=item.value
            walker = maze.walker
            for each direction in maze.cells[item.x][item.y].doors
                if(maze.cells[item.x][item.y].doors[direction].open)
                    nextx=item.x+walker.deltas[direction].x
                    nexty=item.y+walker.deltas[direction].y
                    if ((item.value+1)<maze.cells[nextx][nexty].pathcost) then
                        stack.push({x:nextx,y:nexty,value:(item.value+1)})
                    end if
                end if
            end for
        end if
    end while
End Function
Function ApplyMute(context)
    if context.data.mute then
        context.resources.audioplayer.stop()
    else
        context.resources.audioplayer.play()
    end if
End Function
Function PlaySound(context,soundname)
    if not context.data.mute then
        context.resources.audio[soundname].Trigger(100)
    end if
End Function