Function CreatePlay() As Object
    Return {
        clock: CreateObject("roTimeSpan"),
        doorclock: CreateObject("roTimeSpan")

        start: Function(context) As Void
            NewLevel(context.data,1)
            m.clock.Mark()
            m.doorclock.Mark()
        End Function,

        finish: Function (context) As Void
        End Function,

        draw: Function(context,screen) As Void
            screen.clear(&h000000ff)
            maze = context.data.maze
            columns = context.data.columns
            rows = context.data.rows
            cellwidth = context.data.cellwidth
            cellheight = context.data.cellheight
            unicorn = context.data.unicorn
            dragon = context.data.dragon
            movedragon=m.clock.TotalSeconds()>=2
            if (movedragon)
                m.clock.Mark()
                m.movedragon(context)
            end if
            toggledoor=m.doorclock.TotalSeconds()>=1
            if toggledoor then
                m.doorclock.Mark()
                index = rnd(context.data.toggledoors.count())-1
                door = context.data.toggledoors[index]
                door.open = not door.open
                UpdatePathFinding(context.data)
            end if
            for column=0 to columns - 1
                x=column*cellwidth
                for row=0 to rows - 1
                    y=row*cellheight
                    cell = maze.cells[column][row]
                    screen.drawrect(x,y,cellwidth,cellheight,&h00ffffff)
                    screen.drawrect(x+4,y+4,cellwidth-8,cellheight-8,&h000000ff)
                    if (column=dragon.x and row=dragon.y)
                        screen.drawobject(x+4,y+4,context.resources.bitmaps.dragon)
                    end if
                    if (column=unicorn.x and row=unicorn.y)
                        screen.drawobject(x+4,y+4,context.resources.bitmaps.unicorn)
                    end if
                    if (cell.stairs)
                        screen.drawobject(x+4,y+4,context.resources.bitmaps.stairs)
                    else if (cell.star)
                        screen.drawobject(x+4,y+4,context.resources.bitmaps.star)
                    end if
                    for each direction in cell.doors
                        if (cell.doors[direction].open)
                            if(direction="north")
                                screen.drawrect(x+4,y,cellwidth-8,4,&h000000ff)
                            else if (direction="east")
                                screen.drawrect(x+cellwidth-4,y+4,4,cellheight-8,&h000000ff)
                            else if (direction="south")
                                screen.drawrect(x+4,y+cellheight-4,cellwidth-8,4,&h000000ff)
                            else if (direction="west")
                                screen.drawrect(x,y+4,4,cellheight-8,&h000000ff)
                            end if
                        end if
                    end for
                end for
            end for
        End Function,

        spawnstar: Sub(context)
            if(context.data.starsleft=0)
                for column = 0 to context.data.columns-1
                    for row=0 to context.data.rows-1
                        if (context.data.maze.cells[column][row].stairs)
                            context.data.maze.cells[column][row].stairs=false
                            context.data.maze.cells[column][row].star=true
                            context.data.starsleft++
                        end if
                    end for
                end for
            else
                column = rnd(context.data.columns)-1
                row = rnd(context.data.rows)-1
                while(context.data.maze.cells[column][row].star)
                    column = rnd(context.data.columns)-1
                    row = rnd(context.data.rows)-1
                end while
                context.data.maze.cells[column][row].star=true
                context.data.starsleft++
            end if
        End Sub,

        movedragon:Sub (context)
            maze = context.data.maze
            unicorn = context.data.unicorn
            dragon = context.data.dragon
            currentpath = maze.cells[dragon.x][dragon.y].pathcost
            for each direction in maze.cells[dragon.x][dragon.y].doors
                if(maze.cells[dragon.x][dragon.y].doors[direction].open and maze.cells[dragon.x][dragon.y].neighbors[direction].pathcost<currentpath)
                    dragon.x+=maze.walker.deltas[direction].x
                    dragon.y+=maze.walker.deltas[direction].y
                    if(dragon.x=unicorn.x and dragon.y=unicorn.y)
                        m.spawnstar(context)
                        PlaySound(context,"dragon")
                    end if
                    return
                end if
            end for
        End Sub,

        moveunicorn:Function(context,direction)
            unicorn = context.data.unicorn
            dragon = context.data.dragon
            cell = context.data.maze.cells[unicorn.x][unicorn.y]
            if (cell.doors.doesexist(direction))
                if(cell.doors[direction].open)
                    delta = context.data.maze.walker.deltas[direction]
                    unicorn.x=unicorn.x+delta.x
                    unicorn.y=unicorn.y+delta.y
                    cell = context.data.maze.cells[unicorn.x][unicorn.y]
                    if (cell.stairs)
                        PlaySound(context,"stairs")
                        NextLevel(context.data)
                        m.clock.Mark()
                        m.doorclock.Mark()
                    else if(cell.star)
                        PlaySound(context,"star")
                        cell.star=false
                        context.data.starsleft--
                        if(context.data.starsleft=0)
                            PlaceStairs(context.data)
                        end if
                        UpdatePathFinding(context.data)
                    else
                        if(unicorn.x=dragon.x and unicorn.y=dragon.y)
                            m.spawnstar(context)
                            PlaySound(context,"dragon")
                        end if
                        UpdatePathFinding(context.data)
                    end if
                end if
            end if
        End Function,

        handle: Function (context, event) As String
            if(type(event)="roUniversalControlEvent")
                button = event.GetInt()
                if(button=0)'back
                    return "mainmenu"
                else if(button=2)
                    m.moveunicorn(context,"north")
                else if(button=3)
                    m.moveunicorn(context,"south")
                else if(button=4)
                    m.moveunicorn(context,"west")
                else if(button=5)
                    m.moveunicorn(context,"east")
                else if(button=8)
                    context.data.zoom-=5
                    if context.data.zoom<80 then
                        context.data.zoom=80
                    end if
                else if(button=9)
                    context.data.zoom+=5
                    if context.data.zoom>100 then
                        context.data.zoom=100
                    end if
                else if(button=13)
                    context.data.mute = not context.data.mute
                    ApplyMute(context)
                end if
            end if
            Return "play"
        End Function
    }
End Function