Function CreateContext(port) As Object
    Return {
        resources:CreateResources(port),
        data:CreateData()
    }
End Function