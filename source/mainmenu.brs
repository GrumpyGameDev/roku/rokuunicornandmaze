Function CreateMainMenu() As Object
    Return {
        items:["Play", "How to Play", "About", "Quit"],
        title:"Unicorn and Star",
        fontname:"default",
        backgroundcolor:&h000000FF,
        inactivecolor:&hebebebff,
        activecolor:&hebeb00ff,
        promptColor:&h7575ebff,
        currentIndex:0,

        start: Function(context) As Void
        End Function,

        finish: Function (context) As Void
        End Function,

        draw: Function(context,screen) As Void
            font = context.resources.fonts[m.fontname]
            lineHeight = font.GetOneLineHeight() 
            screen.clear(m.backgroundcolor)
            screen.DrawObject(0,0,context.resources.bitmaps.background)
            y=screen.GetHeight()/2-(lineHeight*5/2)
            index=0
            screen.DrawText(m.title, screen.GetWidth()/2-font.GetOneLineWidth(m.title,screen.GetWidth())/2, y, m.promptColor, font)
            y = y + lineHeight
            for each item in m.items
                offsetX = screen.GetWidth()/2-font.GetOneLineWidth(item,screen.GetWidth())/2
                if (index=m.currentIndex)
                    screen.DrawText(item, offsetX, y, m.activecolor, font)
                else
                    screen.DrawText(item, offsetX, y, m.inactivecolor, font)
                end if
                index=index+1
                y = y + lineHeight
            end for
        End Function,

        handle: Function (context, event) As String
            if(type(event)="roUniversalControlEvent")
                button = event.GetInt()
                if(button=2)'up
                    m.currentIndex=(m.currentIndex+m.items.Count()-1) MOD m.items.Count()
                else if(button=3)'down
                    m.currentIndex=(m.currentIndex+1) MOD m.items.Count()
                else if(button=6)'select
                    if (m.currentIndex=0)
                        return "play"
                    else if (m.currentIndex=1)
                        return "howtoplay"
                    else if (m.currentIndex=2)
                        return "about"
                    else if (m.currentIndex=3)
                        return "confirmquit"
                    end if
                else if(button=0)'back
                    return "confirmquit"
                else if(button=8)
                    context.data.zoom-=5
                    if context.data.zoom<80 then
                        context.data.zoom=80
                    end if
                else if(button=9)
                    context.data.zoom+=5
                    if context.data.zoom>100 then
                        context.data.zoom=100
                    end if
                else if(button=13)
                    context.data.mute = not context.data.mute
                    ApplyMute(context)
                end if
            end if
            Return "mainmenu"
        End Function
    }
End Function